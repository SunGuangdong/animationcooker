# AnimationCooker
AnimationCooker can be used to animate tens to hundreds of thousands of entities by storing animation vertices in a texture which allows the skinned mesh to be treated as a static mesh where the animation is handled in the GPU - greatly reducing draw calls.

There are two main types of texture based GPU animations that are being used today:
  1. *GPU bone skinning* involves baking bone data into a texture to animate bones via a shader or gpu compute buffer. This method taxes the GPU more because each vertex must be run through the bone matrix. Because it is only storing animation curves, texture sizes can be very small and vertex count is not an issue. An LOD system can be used to decrease the number of bones further away and alleviate some of the GPU taxation. It is difficult, but not impossible to develop animation blending with this technique. See: https://github.com/joeante/Unity.GPUAnimation
  2. *Texture vertex baking* involves baking mesh vertex positions into textures. Because the texture size depends on the number of vertices in a mesh, this approach tends to have large textures, so vertex counts must be kept low to avoid too much memory use.  It has very little GPU overhead. With some compression techniques, texture sizes can be improved. This is the strategy that AnimationCooker uses.
  
The main advantage to these two GPU animation techniques is that they are blazing fast - you can animate tens to hundreds of thousands of meshes simultaneously. However, there are sacrifices that must be made in order to achieve this speed. One problem is that it's very difficult to do animation blending.  Another disadvantage is that it is difficult to attach and remove things like weapons and armor. Lastly, you cannot use meshes with a high number of vertices (it's recommended to keep them around 2k or less) -- of course, if you need 10k+ instances, your graphics card will have a hard time handling those high-poly meshes, so animation techniques would be irrelevant.  If you want your characters and animations to look really beautiful up close and you don't need a boatload of instances, then this technique is probably not right for you.  However, if you are instantiating 10k+ instances of zombies at a time and you don't expect them to look amazing up close, then these limitations are likely a decent compromise.  This technique can be combined with LODs and culling to get into the millions.

Animation cooker currently uses ECS, but only very little at the moment. It would be possible to create a small game-object based script to change animations and to add a switch on the gui to disable generation and attachment of the ECS components.

### **Features:**
- Works with Unity 2022.2, URP 14, and DOTS 1.0. (Not tested with SRP or HDRP).
- Vertex animations are stored in a static mesh so that GPU instancing can be used, reducing CPU overhead.
- All animations are combined into the same texture.
- The textures are stored in ARGB32 (32 bit) buffers which are half the size of floating point textures. The vertexes and normals are stored as R11G10B11 (aka X11Y10Z11) format with auto-range-discovery to maximize precision.  Unpacking calculations are done without division to maximize speed.
- Linear interpolation between frames allows baking at lower frame-rates to keep textures small (saving memory).
- AnimationCooker is separated into Editor, Runtime, and ExampleScene so that you can easily add only the necessary components to your projects.
- The GUI has an auto frame-rate/texture-size optimization feature to make it easy to choose the right texture.
- There are options to reset root bone/scale and mesh rotation/scale on models before baking them.
- Bone-meshes are supported (though not thoroughly tested).
- Baking generates a C# database that can be used to access all clip information as well as enums for each clip.
- Automatically generates textures, and adjusted mesh, a material, and a prefab with ECS animation components on it.
- Performs wrapping in order to reduce texture widths and allow more vertices in situations like mobile phones that have a limitation at 1024 (old phones) or 2048. Wrapping calculation does not use division so it's super fast. **new as of 8/23/2022**
- Lit and Unlit shader. The Lit shader is derived from URP SimpleLit. **new as of 8/26/2022**
- Settings get saved per-prefab to make it easier to go back and change previous bakes. **new as of 9/01/2022**
- Authoring tool to store the animation database in a singleton blob-asset so it can be accessed via ISystem with Burst and Jobs **new as of 5/1/2023**

### **Installation:**

The latest push in the master branch is for DOTS 1.0. If you want to use 0.51, there is a tag for v0.51Final that should work.

The shader in 1.0 is built for URP 14, while the shader in 0.51 is for URP 12. The SimpleLit shader is basically If you want to make a shader for a different version (or perhaps make a Lit or HDRP version), you can copy the equivalent shader and HLSL files that make up the current AnimVtxSimpleLit shader and with a merge tool you could make some of the same modifications that I made.  I can't guarantee it will work, but I think there's a decent chance of it. If you do that - send me a copy and I can put them into repo for other people to share.

![menu](Images/menu.png)

The Kitchen is where you'll do your cooking and baking. The other menu items are mainly for debugging and you probably won't use them.

You can choose to either use the baking tools in your main game project by adding the editor tools to your main project, or do the baking in a separate project. Basically, you just copy the Assets/AnimationCooker folder into whatever project you want. If you want the editor in that project, then you're done. If you only want the runtime libraries, then delete the Editor folder. There is also an Assets/ExampleScene folder which you can use to get familiar with an example of how to change animations at runtime, but it's not necessary for anything.  If you are using the editor in a separate project, then after baking you'll need to copy (or export) the contents of the Baked folder and also AnimDb.cs to your game project.

![cooker](Images/cooker.png)

- **Playback Shader:** Baking will create a new material that will use this shader. The default is CookedAnimation/VtxAnimSimpleLit. AnimationCooker will attempt to give the new material similar shader settings to the original material.
- **Baked Output Folder:** Set this to the folder where you want the resulting textures, prefabs, and materials to be placed. It is relative to the Asset folder.
- **Script Output Folder:** Set this to the folder where you want the resulting animation database script to be placed. There is only one animation database for all models. The default is Scripts/Generated. At runtime you will use the animation database to get information about clips and to switch between them.
- **Subscene Object:** Optional - if you set this, any time you bake, the resulting prefab will be added to this subscene. NOTE - Unity does not provide a way to open a subscene in the editor programmatically, so if the subscene is closed when you bake, the prefab won't be added. You can always add prefabs to your subscene anytime.
- **Prefab:** The models that you bake can be either in your assets folder or in the scene. Drag the object you want to bake into this field. It must have an Animation or an Animator attached to it. It must also have a skinned mesh renderer.
- **Format:** This setting determines the frame rate and texture size of the resulting baked animation. The optimized values are auto-discovered so you don't have to waste time trying to figure out the best frame rate for each given texture size. Each rate listed corresponds to the rate that best fills each texture size so that you get the smoothest possible animation for the given texture size.  I find that for most animations, 6 to 12 FPS is the sweet-spot for tradeoff between visual quality and memory use. If the resulting texture's width is wider than 2048, a message will display stating that the texture won't play well on most mobile devices.  If you're targetting mobile devices, you should use modeling software like Blender to reduce the vertex count via a decimation algorithm or merging triangles manually.
- **Animation clips**: Deselect the animations that you want to ignore. The more animations you have, the more texture space will be required, so don't include any extra animations that you won't use.  Keeping animations short also helps.  You can also rename animations here if you wish. If you rename them, the generated database keys and enums will reflect those new names. The names you choose aren't saved permanently in the asset.  See the notes section below for info about choosing animation clip names.
- **Reset Position B4 Bake:** This toggles the reset position flag - see section entitled "Mesh/Vertex Alignment". (default false)
- **Reset Rotation B4 Bake:** This toggles the reset rotation flag - see section entitled "Mesh/Vertex Alignment". (default false)
- **Reset Scale B4 Bake:** This toggles the reset scale flag - see section entitled "Mesh/Vertex Alignment". (default false)
- **Enable Log File:** Check this to output a log file of the bake. This is useful when you need to remember the parameters you originally used to bake. (default true)
- **Declare Enum Clips:** If this box is enabled, all your clip names will be declared as enums during the generation phase. This will allow you to access clips via enum values. It is not necessary however, because you can also access clips via a string in the database. See note below about enum naming.
- **Ignore Bone Meshes:** When this is disabled, if there are meshes on each individual bone, they will be included in the baking process. If you enable this checkbox, it will cause all bone meshes to be ignored.
- **Enable Bone Adjust:** In some circumstances this might be necessary, but most of the time you can leave it disabled.
- **Generated Textures:** This will determine which textures are output during the bake process. For unlit shaders, you should only ever use Position. For lit shaders, it is common to output the normals as well. There is an option for tangent, but as far as I can tell, it's not used by anything.
- **Bake button:** Press this button to generate the baked output and update the animation database.  In most cases, the baking takes only a few seconds. However, during baking, the animation clip database is generated (a C# file), and Unity will want to recompile its assets. Refreshing assets takes several seconds. After the asset refresh, you will see "Asset recompilation finished." at the end of the Result text field.
- **Prediction:** This field displays information about the expected output.
- **Last Bake Result:** After you press the cook button, this field will display information about the resulting baked output.

### **How animations are played:**
The animations are played by the CPU in AnimationSystem (This method is currently ECS only).  Unlike GPU playback modes, this method allows you to know when clips end (which is important for stopping animations when they finish).  There is a very basic scheme to control animations via AnimationCmdData.  See the Code section below to learn how to use it. It currently does not have a state-machine type system or blending like Mechanim... it's very basic, but works well in many zombie-type games.

### **Mesh/Vertex Alignment:**
Unfortunately, I couldn't figure out a way to auto-determine if a particular mesh needs its positions, rotations, or scaling reset to zero before baking. There are three checkboxes: "Reset Position B4 Bake", "Reset Rotation B4 Bake", and "Reset Scale B4 Bake" that will toggle the flags that reset to origin/defaults before baking.  Some models+animations will require a specific combination of the two boxes to be checked, while others will work regardless of what you check or uncheck. When you select your baked prefab in the heirarchy, there will be a red outline of the mesh.

Additionally, it is important to make sure that the **root bone** is selected on your skinned mesh renderer.  Sometimes it is difficult to tell which bone is meant to be root bone for a particular model. I have found that a good way to determine which bone to use is by dragging the model/fbx into the scene - the root bone usually automatically gets set.  

![rootbone](Images/rootbone.png)

I find that the best way to work with models is by turning the model/fbx into a prefab - if it is a legacy animation it will automatically add the Animation component and fill it out for you. If it's an animation with a controller it will automatically add an Animator component and then you can set the controller for it.

Here is an example of an incorrectly aligned model due to rotation. Notice how the red outline doesn't match what the shader is drawing:

![mismatch](Images/mismatch.png)

And here is the same model after toggling the "Reset Rotation B4 Bake"

![aligned](Images/aligned.png)

So to sum it up... Bake your animations. If the red mesh outline doesn't match, toggle the appropriate box(es) and bake again.

### **Notes:**
- **Unique names - IMPORTANT:** The prefabs/models that you bake should have unique names. They are used as keys in a dictionary, so if you were to bake two models that both had the exact same name, the second one would overwrite the first.
- **Declare Clip Enums** If you enabled the setting to declare animation enum clips, then be aware that the enums generated may not match your string exactly. An attempt will be made to rename the enum such that it doesn't violate any C# rules.  There are no guarantees, however, so use at your own risk. I suggest using simple names like "Walk", "Idle", or "Run". You should be careful because if your enums need to be renamed, then you should rebake to make sure the strings match the database.
- **Bilinear Filtering:** It is very important that your textures (position and normal) do not have a bilinear filter enabled (which is the default).  The baking script disables the bilinear filter after it builds the textures, but if for some reason you reimport you might lose that setting. You can change the "Filter Mode" field in the inspector tab by selecting the texture in the Project tab.  Set it to "Point".  Whenever the bilinear filter is on, you'll noticed that the first and last animated frames will be garbage.

![texprops](Images/tex_props.png)

### **Material Settings:**

![lit shadersettings](Images/litshader_settings.png)

When baking, AnimationCooker will attempt to setup the resulting material so that the shader options match the original material - so for example if you setup a bump map in the original material, it will also be set in the generated material.  You can change these at runtime, if for example, you want to modify the animation speed.

- **\_PosMap**: Texture that holds vertex position and clip information. This will be generated in your output folder.
- **\_NmlMap**: Texture that holds normals. This will be generated in your output folder.
- **\_TanMap**: Texture that holds tangents. This will be generated in your output folder. (not normally used).
- **\_CurTime**: I haven't had time to test this. It is a per-instance property.
- **\_ClipIdx**: This is a per-instance property that you can use to specify which clip you want to play.
- **\_SpeedInst**: This is a per-instance property that you can use to speed up or slow down the animation.
- **\_SpeedMat**: This is a per-material property that you can use to speed up or slow down the animation for all instances using the material.
- **\_UseNormalMap:** Selecting this will put in a compiler directive to use the normal map. (default - enabled for the simple-lit shader)
- **\_OnlyUsePosMap:** Selecting this will put in a compiler directive to ignore the normal mapping code. If you don't need normal or tangent mapping, this will make it run slightly faster.
- **Enable GPU Instancing**: I'm not really sure if this checkbox makes a difference because whenever I have it checked and display a bunch of entities, "Saved by Batching" is still zero, and it doesn't seem to make a difference in performance.  I believe that instancing is always enabled whether or not this is checked. ¯\_(ツ)_/¯

Animation Clip Database:

**Generated/AnimDb.cs** is a script that is auto-generated and updated whenever you press the "Cook" button. It contains a function that fetch the animation information for all models and all animations.  This clip information contains things like start-frame, end-frame, frame-count, and clip-index.  There currently isn't a way to remove items, but it doesn't really hurt to have unused items in there (for example, if you baked a model but no longer use it). If for some reason the file grows really huge with unwanted data you could manually erase some of the lines.

Output subfolder:

![bakedfiles](Images/baked_files.png)

- The **.material.asset** file contains the material that points to the texture files and shader needed to playback the baked vertex animations.
- The **.normTex.asset** and **.posTex.asset** files are the normal and position textures, respectively.
- The **\_Baked.prefab** file holds a prefab that is allready setup for ECS use.
- The **.mesh.asset** mesh is nearly identical to the original mesh, but it won't have bone information and its coordinates and rotations will be reset to defaults.

### **Runtime Code:**
The most important thing is that you add an **AnimationClipConverter** component to a gameobject (this is automatically added to the baked prefab created by the "Cook" function). If you're creating your objects in Pure ECS instead, look at the code inside of **AnimationClipConverter**.  The converter adds (and populates) an **AnimationStateData** component that holds current animation state data.  It also adds some components that let you change some per-instance shader properties such as **MaterialClipIndex** and **MaterialSpeed**.

Animation Clip Converter (Authoring Component):

![converter](Images/converter.png)

There is only one parameter on this authoring component, and it is automatically filled out if you use the prefab generated by the "Cook" function.  It's a string that must correspond to the name of the original prefab that you baked.

To get details about a clip you can use the database called **AnimDb**, which is loaded with the auto-generated data contained in **AnimDb.cs**.

Unfortunately, ECS won't let you use AnimationDatabase in a burst function because you can't use classes within burst.
To use the animation database with burst you should place a single gameobject in a subscene that has an AnimationDbAuthoring on it.
This will allow you to fetch the animation data as a singleton of type AnimDbRefData.  

``` csharp
[BurstCompile]
public partial struct MyJob : IJobEntity
{
    [ReadOnly] public AnimDbRefData Db;
	
    // matches all animation entities in the scene
    [BurstCompile]
    public void Execute(ref AnimationCmdData cmd, in AiActionData action, in AnimationStateData state)
    {
        // fetch the currently playing clip
        AnimDbEntry clip = Db.GetClip(state.ModelIndex, state.CurrentClipIndex);
        UnityEngine.Debug.Log($"The current model {clip.ModelName} is playing the {clip.ClipName} clip which is {clip.GetLength()} seconds long.");
    }
}

[BurstCompile]
public partial struct MySystem : ISystem
{
    void OnCreate(ref SystemState state)
    {
        // ensures that animation database singleton exists before calling update
        state.RequireForUpdate<AnimDbRefData>();
    }
	
    [BurstCompile]
    void OnUpdate(ref SystemState state)
    {
        MyJob job = new MyJob();
        job.Db = SystemAPI.GetSingleton<AnimDbRefData>(); // fetch the animation database singleton
        job.ScheduleParallel();
    }
}
```

Using the database from a monobehavior or an unbursted system is easy - just fetch the singleton monobehavior for it:

``` csharp
var db = AnimationDatabase.GetDb();
foreach (var model in db) {
  foreach (var clip in model.Value) {
    Debug.Log("Model: " + model.Key + ", Clip: " + clip.Key + ", Index: " + clip.Index);
  }
}
```

You change the current clip by setting a command.  
- PlayOnce will play the clip once and then go back to playing the forever-clip (this is useful for actions like Attack, Stagger, or Jump).
- SetPlayForever will set the forever-clip (this is useful for actions like Idle, Run, or Walk).
- PlayOnceAndStop will play the specified clip and then stop the play loop (this is useful for death animations).
Here's an example:

``` csharp
EntityManager.SetComponentData<AnimationCmdData>(entity, new AnimationCmdData { Cmd = AnimationCmd.SetPlayForever, ClipIndex = (byte)AnimDb.Horse.Idle });
```

Here's how you could double the animation playback speed:

``` csharp
EntityManager.SetComponentData<MaterialSpeed>(entity, new MaterialSpeed { Multiplier = 2f });
```

### **Linear Interpolation:**
Here is the original animation at 30fps:

![30fps](Images/30fps.gif)

Here it is at 11fps without interpolation:

![10fps](Images/10fps.gif)

Here it is at 11fps with interpolation:

![10fps](Images/10fps_interpolated.gif)

The 11fps version is 3,076KB vs 12,292KB for the 30fps one, so it's about 1/4 the size.

### **Example Scene:**

![fullwindow](Images/full_window.png)

The example scene contains a small UI and a spawner script so you can spawn a large number of entities in a grid.

The Spawner script will query for any entities in the scene that have an AnimationStateData attached to it (which is automatically added to your baked prefab by the baking/cooking script.)  It will spawn all found entities in equal numbers and cycle through the animations for each one.

### **TODO:**
1. A GameObject based animation script might be useful for people who aren't using ECS. It should be pretty easy to do by using the animation database which is already generated and easy to use. This is low on my priority list since I won't really need it.
2. Add basic animation blending support?
3. If you modify the prefab, the only way to refresh the Animation Kitchen window is by dragging the prefab into the Prefab field again.
4. I think the unlit shader might not be working - I haven't tested it recently.
5. Changing material property colors on the simple-lit version of the shader stopped working.

### **Credits and resources:**
- The guys in this thread: https://forum.unity.com/threads/graphics-drawmeshinstanced.537927/ (zulfajuniadi, Arathorn_J, elJoel, DreamingImLatios, jdtec, and Shane_Michael)
- TheLordBaski's video tutorial: https://www.youtube.com/watch?v=KUuuXowdYyM&ab_channel=TheLordBaski
- Jiadong's coding blog: https://www.cnblogs.com/murongxiaopifu/p/7250772.html
- The programmersought article: https://www.programmersought.com/article/5083122695/
- Andre Sato's article: https://medium.com/tech-at-wildlife-studios/texture-animation-techniques-1daecb316657
- zulfajuniadi's Animation Texture Baker (dev branch) (forked from sugi-cho): https://github.com/zulfajuniadi/Animation-Texture-Baker
- sugi-cho's Animation Texture Baker: https://github.com/sugi-cho/Animation-Texture-Baker
- bgolus: https://forum.unity.com/members/bgolus.163285/
- gussk (Gustav): https://forum.unity.com/members/gussk.114410/


